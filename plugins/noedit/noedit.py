# -*- coding: utf-8 -*-
import time
import random
from datetime import datetime


crontable = []
outputs = []

'''
{
    u'text': u'asdf',
    u'ts': u'1448952297.002401',
    u'user': u'U0FJK29FW',
    u'team': u'T0FJF9QAE',
    u'type': u'message',
    u'channel':u'C0FJEGF9C'
}
C0FJEGF9C
'''
ZSPACE = ZERO_WIDTH_NO_BREAK_SPACE = u'\uFEFF'
MODIFY_FORMAT = u'```[수정] <@%s>: %s => %s```'


def push_msg(channel, msg):
    if len(msg) > 7000:
        outputs.append([channel, u'[ERR] Too long! (max: 7000)'])
    else:
        # 콩시 이스터에그
        d = datetime.now()
        if d.hour % 12 == 2 and d.minute == 22:
            outputs.append([channel, msg])

        outputs.append([channel, msg])


def isIn(obj, attrs):
    sa = attrs.split('.')
    eo = obj
    for attr in sa:
        if attr in eo:
            eo = obj
        else:
            return False
    return True


def escape_backtick(str):
    r = str.replace('``', '`' + ZSPACE  + '`')
    return r


def dododo(data):
    prev_msg = escape_backtick(data['previous_message']['text'])
    msg = escape_backtick(data['message']['text'])
    push_msg(data['channel'], MODIFY_FORMAT % (data['previous_message']['user'], prev_msg, msg))


def process_message(data):
    try:
        if 'message' in data and ('attachments' in data['message'] or 'file' in data['message']):
            return

        if 'type' in data and data['type'] == 'message':
        # if data['channel'] == "C0FJEGF9C" or data['channel'] == "C0FJQ6Q69":
            if 'subtype' in data and data['subtype'] == 'message_changed':
                if data['message']['text'] == '!':
                    dododo(data)
                elif (data['previous_message']['user'] == 'U0FJC55KM' and random.randint(1,10) >= 0):
                    dododo(data)
                elif (data['previous_message']['user'] == 'U0FMR7VN1' and random.randint(1,8) == 1):
                    pass
                elif random.randint(1, 3) == 1:
                    pass
                else:
                    pass
                    # dododo(data)
    except Exception as e:
        print e
