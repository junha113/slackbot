# -*- coding: utf-8 -*-
import time
import random
from marshmallow import pprint


crontable = []
outputs = []

'''
{
    u'text': u'asdf',
    u'ts': u'1448952297.002401',
    u'user': u'U0FJK29FW',
    u'team': u'T0FJF9QAE',
    u'type': u'message',
    u'channel':u'C0FJEGF9C'
}
C0FJEGF9C
'''


do_list = {}
help_msg = u'''```히익-찬을 차냥합니다!
수정충을 박멸합니다!

[Commands]
~help | ~heek                       히익-찬을 차냥하고 도움말을 출력합니다.
~star <character> <size (1 ~ 5)>    육망성을 출력합니다.
~set <src> <desc>                   ~<src>를 입력했을 때 <desc>를 출력합니다.
~rm <src>                           ~<src> 명령을 삭제합니다.
~가위 | ~바위 | ~보                  봇과 가위바위보 게임을 합니다.
~?<query>                            Wolframalpha에 질의를 합니다.
```'''

rcp = {
    u'바위': [u'보', u'가위', u'바위'],
    u'가위': [u'바위', u'보', u'가위'],
    u'보': [u'가위', u'바위', u'보']
}
rcp_msg = [u'*LOSE* 내가이김 ㅅㄱ', u'*WIN* 당신이 이겼습니다!', u'*DRAW* 비겼네여..']


def push_msg(channel, msg):
    if len(msg) > 7000:
        outputs.append([channel, u'[ERR] Too long! (max: 7000)'])
    else:
        outputs.append([channel, msg])


def create_sixstar(EM_FILL, EM_BLANK, N):
    res = u""
    for i in range(N * 4):
        a = i * 2 + 1
        b = (N * 4 - i - 1) * 2 + 1

        if a >= N * 2 * 3:
            a = 0
        if b >= N * 2 * 3:
            b = 0

        c = b
        if a > b:
            c = a

        d = 3 * N - (c / 2)
        if d < 0:
            d = 0

        str = EM_BLANK * d
        str = str + EM_FILL * c

        res = res + str + "\n"

        if i == N * 2 - 1:
            res = res + EM_BLANK * (N + 1)
            res = res + EM_FILL * (N * 4 - 1) + u'\n'

    return res.strip()


def process_message(data):
    pprint(data)
    print '\n\n'
    try:
        if data['channel'] == "C0FJEGF9C" or data['channel'] == "C0FJQ6Q69":
            # print u"%s: %s" % (data['user'], data['text'])

            # JIYONG
            if u"@U0FJC55KM" in data['text']:
                # print "JIYONG!!"
                push_msg(data['channel'], u":jiyong:: 테:star:콩 차냥해!!")

            # HYEONSU
            if u"@U0FJE00D7" in data['text']:
                # print "HYEONSU!!"
                push_msg(data['channel'], u":hs:: 촵촵")

            if data['text'].startswith('~'):
                sp = data['text'].split(' ')

                # HELP
                if sp[0].lower() == '~help' or sp[0].lower() == '~heek':
                    push_msg(data['channel'], help_msg)

                if sp[0] == u'~가위' or sp[0] == u'~바위' or sp[0] == u'~보':
                    res = random.randint(0, 2)  # BOT: WIN LOSE DRAW

                    push_msg(data['channel'], u'BOT: %s\nYOU: %s\n%s' %
                             (rcp[sp[0][1:]][res], sp[0][1:], rcp_msg[res]))

                # STAR
                if sp[0].lower() == '~star':
                    em_fill = sp[1]
                    n = int(sp[2])

                    # print ">> SIXSTAR %s x%d" % (em_fill, n)

                    if n > 5:
                        push_msg(data['channel'], u"너무 커요! (max: 5)")
                    else:
                        res = create_sixstar(em_fill, ':blank:', n)

                        push_msg(data['channel'], res)

                ### CUSTOMIZE ###
                # SET
                if sp[0].lower() == '~set':
                    do_list.update({sp[1]: sp[2]})
                    push_msg(data['channel'],
                             u"Say *%s* when *%s*" % (sp[2], sp[1]))

                # REMOVE
                if sp[0].lower() == '~rm':
                    sp = data['text'].split(' ')

                    if sp[1] in do_list:
                        do_list.pop(sp[1])
                        push_msg(data['channel'],
                                 u"Removed *%s*" % (sp[1]))

                # DO DO DO!
                if data['text'][1:] in do_list:
                    push_msg(data['channel'], do_list[data['text'][1:]])

            # if u"냠냠" in data['text']:
            #     push_msg(data['channel'], u"재원")
    except Exception as e:
        print e
        print '\n\n'

