# -*- coding: utf-8 -*-
import time
import tungsten


crontable = []
outputs = []

'''
{
    u'text': u'asdf',
    u'ts': u'1448952297.002401',
    u'user': u'U0FJK29FW',
    u'team': u'T0FJF9QAE',
    u'type': u'message',
    u'channel':u'C0FJEGF9C'
}
C0FJEGF9C
'''


client = tungsten.Tungsten('37QTEK-43WYQUP7J9')
params = {'format': ['plaintext', 'image']}


def push_msg(channel, msg):
    if len(msg) > 7000:
        outputs.append([channel, u'[ERR] Too long! (max: 7000)'])
    else:
        outputs.append([channel, msg])


def wfa_search(query, channel):
    print ">> Query: " + query

    r = client.query(query, params)
    print "  Success: " + str(r.success)
    print "  Error: " + str(r.error)

    if r.success is False and r.error is None:
        t = None
        try:
            t = r.xml_tree.getroot().find('didyoumeans').find('didyoumean').text
        except Exception as e:
            print e

        if t:
            print ">> Re-Query"
            wfa_search(t, channel)
        else:
            push_msg(channel, 'No result')
            print "  No result"
        return

    text = ''
    img = ''
    try:
        text = r.pods[1].root.find('subpod').find('plaintext').text
    except Exception as e:
        print e

    try:
        img = r.pods[1].root.find('subpod').find('img').attrib['src']
    except Exception as e:
        print e

    print "  Text: " + unicode(text)
    print "  Img: " + unicode(img)

    push_msg(channel, 'Query: *%s*' % query)
    if text:
        push_msg(channel, text)
    if img:
        push_msg(channel, img)

    if not text and not img:
        push_msg(channel, 'No result')


def process_message(data):
    try:
        if data['channel'] == "C0FJEGF9C" or data['channel'] == "C0FJQ6Q69":
            # print u"%s: %s" % (data['user'], data['text'])

            if data['text'].startswith('~?') and len(data['text']) > 1:
                wfa_search(data['text'][2:], data['channel'])

    except Exception as e:
        print e
